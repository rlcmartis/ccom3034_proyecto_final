/******WIRELESSNETWORK.H*************************************************
 *      Este es el "hearder" de la clase wirelessNetwork. Basicamente   *
 * es un plano de tamano grid que posee un grafo (clase myGraph) sin    *
 * direccion. Esta tiene dos funciones primordiales y requeridas para   *
 * el proyecto las cuales son TopologyControl() y CompassRouting().     *
 * Posee otras funciones y miembro que fueron creadas para hacer        *
 * posible su funcionalidad.                                            *
 *                                                                      *
 ************************************************************************
 ************************************************************************
 * est. Ramon L Collazo     801-12-1480     rlcmartis@gmail.com         *
 * Prof. Arce Nazario       Estructura de Datos       CCOM-3034         *
 ************************************************************************/

#ifndef WIRELESSNETWORK_H
#define WIRELESSNETWORK_H

#include "mygraph.h"
#include <stdlib.h>
#include <ctime>

class wirelessNetwork{
    private:
        float grid; //El size del network (grid x grid)
        myGraph G; //Un grafo donde cada vertice es un
        //dispositivo y cada arista es su coneccion

        vector<bool> visitados;

    public:
        //Crea una cuadricula 10 x 10 (con 500 nodos)
        wirelessNetwork();

        //Crea una cuadricula size x size (con n nodos)
        wirelessNetwork(float size, int n);

        //Calcula y devuelve la distancia entre dos vertices
        double dista(Vertex A, Vertex B);

        //Crea documento .dot
        void dot(string name);

        //Despliega el grafo en pantalla en formato propio del autor
        void display();

        //Devuelve angulo del vertice X con la ley de cosenos
        double angle(Vertex Y, Vertex X, Vertex Z);

        //Busca el de mayor grado y el promedio de grados
        void aboutDeg(int &maxD, int &averageD);

        //Devuelve el numero de vertices en la red
        int getNumberVertices();

        //Hace un push a la vez que devuelve el vector
        vector<int> merge(vector<int> all, int piece);

        //Reduce el numero de aristas haciendo que cada nodo tenga
        //solo aquellos nodos mas cercanos en su AWList
        void topologyControl();

        //Funcion de ayuda para la recursion, solo inicializa a
        //falso el vector de booleanos e invoca al verdadero
        //compass routing
        vector<int> compassRouting(int s, int t);

        //Es invocado por una funcion "de ayuda" para la recursion
        //Halla el camino mas corto del nodo s a t por medio de
        //compass routing; esto es, buscando los nodos n que hagan
        //el angulo nst mas corto (donde s es el vertice)
        vector<int> compassRouting2(int s, int t);
};

#endif // WIRELESSNETWORK_H
