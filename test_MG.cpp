#include "wirelessnetwork.h"
#include "mygraph.h"
#include <iostream>
#include <cassert>
#include <stdlib.h>
#include <ctime>
using namespace std;

void test_addVertex(){
    myGraph A;
    A.addVertex(5,5);
    A.addVertex("Nodo",3,2);
    assert(A.getNumberVertices() == 2);
}

void test_findNodeIndex(){
    myGraph A;
    A.addVertex("Uno",1,1);
    A.addVertex("Uno",5,5);
    assert(A.findNodeIndex("Uno") == 0);
    assert(A.findNodeIndex("Dos") == -1);
}

void test_getData(){
    myGraph A;
    A.addVertex("Uno",0,0);
    A.addVertex(1,1);

    assert(A.getData(0)=="Uno");
    assert(A.getData(1)=="");
}

void test_getNumberEdges(){
    myGraph A;
    A.addVertex("Uno",0,0);
    A.addVertex("Dos",1,2);
    A.addVertex("Tres",2,4);
    A.addVertex("Cuatro",4,4);
    A.addEdge(0,1,9);
    A.addEdge(1,2,9);
    A.addEdge(2,3,9);
    A.addEdge(3,0,9);

    assert(A.getNumberEdges()==4);
    assert(A.getNumberEdges(0)==2);
}

void test_getNeighbors(){
    myGraph A;
    A.addVertex("Uno",0,0);
    A.addVertex("Dos",1,2);
    A.addVertex("Tres",2,4);
    A.addVertex("Cuatro",4,4);

    A.addEdge(0,1,9);
    A.addEdge(1,2,9);
    A.addEdge(2,3,9);
    A.addEdge(3,0,9);
    A.addEdge(0,2,9);
    A.addEdge(1,3,9);
    assert((A.getNeighbors(3)).size()==3);
    assert((A.getNeighbors(0))[2]==2);
}

void test_getNumberVertices(){
    myGraph A;
    assert(A.getNumberVertices()==0);

    A.addVertex("One",0,1);
    A.addVertex("Two",1,2);
    A.addVertex("Three",2,3);

    assert(A.getNumberVertices()==3);
}

void test_addEdge(){
    myGraph A;
    A.addVertex("Uno",1,1);
    A.addVertex("Dos",2,2);
    A.addVertex("Tres",3,3);

    A.addEdge(0,1,5);
    A.addEdge("Tres","Dos",5);
    assert(A.getNumberEdges()==2);
    assert((A.getNeighbors(1)).size() == 2);
}

void test_getAdjacent(){
    myGraph A;
    A.addVertex("Uno",1,1);
    A.addVertex("Dos",2,2);
    A.addVertex("Tres",3,3);

    A.addEdge(0,1,7);
    A.addEdge(1,2,7);
    assert(A.getAdjacent(1,1)==2);
}

void test_deleteVertex(){
    myGraph A;
    A.addVertex("Uno",0,0);
    A.addVertex("Dos",1,2);
    A.addVertex("Tres",2,4);

    assert(A.getNumberVertices()==3);
    A.deleteVertex("Dos");
    assert(A.getNumberVertices()==2);
    assert(A.getData(1)=="Tres");
    A.deleteVertex(0);
    assert(A.getNumberVertices()==1 && A.getData(0)=="Tres");
}

void test_deleteEdge(){
    myGraph A;
    A.addVertex("One",0,1);
    A.addVertex("Two",1,2);
    A.addVertex("Three",2,3);

    A.addEdge("One", "Three", 2);
    assert(A.getNumberEdges()==1);
    assert(A.getAdjacent(2,0)==0);
    A.addEdge(1,0,1);
    assert(A.getNumberEdges()==2);
}

int main(){
    test_addVertex();
    test_findNodeIndex();
    test_getData();
    test_getNumberEdges();
    test_getNeighbors();
    test_getNumberVertices();
    test_addEdge();
    test_getAdjacent();
    test_deleteVertex();
    test_deleteEdge();

//    wirelessNetwork A;
//    A.dot("antes.dot");
//    A.topologyControl();
//    A.dot("despues.dot");

//    //Para correr graphviz en Windows luego de instalar,
//    //obtener permisos y guardar despues.dot en Graphviz\bin:
//    //cd C:\Program Files (x86)\Graphviz2.34\bin
//    //dot -Kfdp -n -Tpdf -o despues.pdf despues.dot

    return 0;
}

