/******EXPERIMENTS.CPP***************************************************
 *      En este codigo fuente encontrara los experimentos preparados    *
 * para el proyecto de wirelessNetwork elaborado para la clase de       *
 * estructura de datos (CCOM-3034), primer semestre 2013-2014.          *
 ************************************************************************
 ************************************************************************
 * est. Ramon L Collazo     801-12-1480     rlcmartis@gmail.com         *
 * Prof. Arce Nazario       Estructura de Datos       CCOM-3034         *
 ************************************************************************/

#include "wirelessnetwork.h"
#include "mygraph.h"
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
using namespace std;

void line(){
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
}

void degreesInfo(wirelessNetwork A){
    int maxD, averD;

    A.aboutDeg(maxD, averD);
    cout << A.getNumberVertices() << " vertices in a 10x10 grid:\n"
         << "\tMaximum Degree: " << maxD
         << "  Average Degree: " << averD << endl;

    A.topologyControl();
    A.aboutDeg(maxD, averD);
    cout << "And with Topology Control:\n"
         << "\tMaximum Degree: " << maxD
         << "  Average Degree: " << averD << endl << endl;
    line(); cout << endl;
}

void Experiment1(){
    cout << "~~~~RUNNING EXPERIMENT #1"; line(); cout << endl;
    wirelessNetwork A, B(10,550), C(10,600),
            D(10,650), E(10,700), F(10,750),
            G(10,800), H(10,850), I(10,900),
            J(10,950);

    degreesInfo(A);    degreesInfo(B);
    degreesInfo(C);    degreesInfo(D);
    degreesInfo(E);    degreesInfo(F);
    degreesInfo(G);    degreesInfo(H);
    degreesInfo(I);    degreesInfo(J);
    cout << endl;
}

void Experiment2(wirelessNetwork K, int *s, int *t){
    cout << "~~~~RUNNING EXPERIMENT #2"; line(); cout << endl;

    for (int i=0; i<10; i++){
        vector<int> compassed = K.compassRouting(s[i], t[i]);

        if (compassed.size()==1 && compassed[0]==t[i]){
            cout << "El camino de " << s[i] << "->" << t[i]
                 <<" es nulo pues es el mismo vertice.\n";
        }
        else if (compassed[0]==t[i]){
            cout << "Camino de " << s[i] << "->" << t[i]
                 << " es: ";
            cout << compassed.size() << endl;
        }
        else{
            cout << "No hay camino de " << s[i] << "->" << t[i] <<endl;
        }
    }
    cout << endl;
}

void Experiment3(wirelessNetwork K, int *s, int *t){
    cout << "~~~~RUNNING EXPERIMENT #3"; line(); cout << endl;
    K.topologyControl();

    for (int i=0; i<10; i++){
        vector<int> compassed = K.compassRouting(s[i], t[i]);

        if (compassed.size()==1 && compassed[0]==t[i]){
            cout << "El camino de " << s[i] << "->" << t[i]
                 <<" es 0 pues es el mismo vertice.\n";
        }
        else if (compassed[0]==t[i]){
            cout << "Camino de " << s[i] << "->" << t[i]
                 << " es: ";
            cout << compassed.size() << endl;
        }
        else{
            cout << "No hay camino de " << s[i] << "->" << t[i] <<endl;
        }
    }
    cout << endl;
}

int main(){
    srand(time(NULL));

    Experiment1();

    wirelessNetwork K(10,1000);
    int s[10], t[10], ss, tt;
    for (int i=0; i<10; i++){
        ss = rand() % 1000;
        tt = rand() % 1000;
        s[i] = ss;
        t[i] = tt;
    }

    Experiment2(K,s,t);
    Experiment3(K,s,t);

    return 0;
}
