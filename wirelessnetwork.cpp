/******WIRELESSNETWORK.CPP***********************************************
 *      Esta es la implementacion de la clase wirelessNetwork.          *
 *                                                                      *
 ************************************************************************
 ************************************************************************
 * est. Ramon L Collazo     801-12-1480     rlcmartis@gmail.com         *
 * Prof. Arce Nazario       Estructura de Datos       CCOM-3034         *
 ************************************************************************/

#include "wirelessnetwork.h"
#include "mygraph.h"
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include <fstream>
#include <string>
using namespace std;

//Calcula y devuelve la distancia entre dos coordenadas
double wirelessNetwork::dista(Vertex A, Vertex B){
    return sqrt((B.x - A.x)*(B.x - A.x) + (B.y - A.y)*(B.y - A.y));
}

//Crea una cuadricula 10 x 10 (con 500 nodos)
wirelessNetwork::wirelessNetwork(){
    grid = 10;
    double x, y, w;

    for (int i=0; i<500; i++){
        //PROBLEMA: Hace numeros randoms pero
        //cada vez que se crea un wirelessNetwork
        //se crean los mismos numeros, el mismo grafo.

        x = ((double)rand() / RAND_MAX) * grid;
        y = ((double)rand() / RAND_MAX) * grid;

        G.addVertex(x,y);

        //Recorremos todos los vertices calculando la distancia
        //con el nuevo nodo creado. De ser una distancia menor
        //o igual a 1 se crea una arista
        for (int i = 0; i < (int)G.Vertices.size()-1; i++){
            w = dista(G.Vertices.back(), G.Vertices[i]);
            if (w <= 1)
                G.addEdge(i, (int)G.Vertices.size()-1, w);
        }
    }
    vector<bool> f(500,false);
    visitados = f;
}

//Crea una cuadricula size x size (con n nodos)
wirelessNetwork::wirelessNetwork(float size, int n){
    grid = size;
    double x, y, w;

    for (int i=0; i<n; i++){
        //PROBLEMA: Hace numeros randoms pero
        //cada vez que se crea un wirelessNetwork
        //se crean los mismos numeros, el mismo grafo.

        x = ((double)rand() / RAND_MAX) * grid;
        y = ((double)rand() / RAND_MAX) * grid;

        G.addVertex(x,y);

        //Recorremos todos los vertices calculando la distancia
        //con el nuevo nodo creado. De ser una distancia menor
        //o igual a 1 se crea una arista
        for (int i = 0; i < (int)G.Vertices.size()-1; i++){
            w = dista(G.Vertices.back(), G.Vertices[i]);
            if (w <= 1)
                G.addEdge(i, (int)G.Vertices.size()-1, w);
        }
    }
    vector<bool> f(n,false);
    visitados = f;
}

//Crea documento .dot
void wirelessNetwork::dot(string name){
    ofstream A(name.c_str());

    A << "graph G {\noverlap=false;\nsize = \""
      << grid << "," << grid << "\";" << endl;
    A << "node [shape=square, fixedsize=true, "
         << "fontsize=5, width=.10, height=.10];\n";
    for(int i=0; i<(int)G.Vertices.size(); i++)
        A << i << " [ pos = \"" << G.Vertices[i].x
          << "," << G.Vertices[i].y << "!\", label=\""
          << G.Vertices[i].data << "\"  ]\n";
    for (int i=0; i<(int)G.Vertices.size(); i++)
        for(int j=0; j<(int)G.Vertices[i].AWList.size(); j++)
            if (G.Vertices[i].AWList[j].adjacent > i)
                A << i << "--"
                  << G.Vertices[i].AWList[j].adjacent << ";\n";
    A << "}";
    A.close();
}

//Despliega el grafo en pantalla en formato propio del autor
void wirelessNetwork::display(){
    G.display();
}

//Devuelve angulo del vertice X con la ley de cosenos
double wirelessNetwork::angle(Vertex s, Vertex t, Vertex x){
    double A, B, C;
    A = dista(t,x);
    B = dista(t,s);
    C = dista(s,x);
    return (B*B + C*C - A*A)/(2*B*C);
}

//Busca el de mayor grado y el promedio de grados
void wirelessNetwork::aboutDeg(int &maxD, int &averageD){
    int total = 0, tmp;

    //Asumimos que el primer vertice tiene el mayor grado
    maxD = G.getNumberEdges(0);
    //int imaxD = 0;

    total += maxD;

    //Vamos por los demas vertices verificando si hay uno
    //mayor, siempre que haya uno mayor, sustituimos maxD
    //por el nuevo valor
    for (int i=1; i<(int)G.Vertices.size(); i++){
        tmp = G.getNumberEdges(i);
        if (tmp>maxD){
            maxD = tmp;
            //imaxD = i;
        }
        total += tmp;
    }

    averageD = (int)(total/G.getNumberVertices());
}

//Devuelve el numero de vertices en la red
int wirelessNetwork::getNumberVertices(){
    return G.getNumberVertices();
}



//Reduce el numero de aristas haciendo que cada nodo tenga
//solo aquellos nodos mas cercanos en su AWList
void wirelessNetwork::topologyControl(){
    int a, b, c;

    vector<int> paraBorrar, aNeigh, bNeigh;

    //Vamos por todos x nodos del grafo
    for (int x=0; x<(int)G.Vertices.size(); x++){
        a = x; //a es el indice de un nodo
        aNeigh = G.getNeighbors(a); //aNeigh son los vecinos de a

        //Pasamos por los nodos adyacentes de a
        for (int y=0; y<(int)G.Vertices[a].AWList.size(); y++){
            b = aNeigh[y]; //b es adyacente de a
            bNeigh = G.getNeighbors(b); //bNeigh son los vecinos de b

            //Buscamos cuales nodos son vecinos de u y v
            for (int z=0; z<(int)G.Vertices[b].AWList.size(); z++){
                c = bNeigh[z];

                if ((dista(G.Vertices[a],G.Vertices[c])!=0 && dista(G.Vertices[b],G.Vertices[c])!=0) &&
                        dista(G.Vertices[a],G.Vertices[c]) < dista(G.Vertices[a],G.Vertices[b]) &&
                        dista(G.Vertices[b],G.Vertices[c]) < dista(G.Vertices[a],G.Vertices[b])){
                        paraBorrar.push_back(b);//indice del que debe eliminarse
                }
            }
        }

        for (int i=0; i<(int)paraBorrar.size(); i++){
            G.deleteEdge(paraBorrar[i], a);
        }

        paraBorrar.clear();
    }
}

//Hace un push a la vez que devuelve el vector
vector<int> wirelessNetwork::merge(vector<int> all, int piece){
    all.push_back(piece);
    return all;
}

//Funcion de ayuda para la recursion, solo inicializa a
//falso el vector de booleanos e invoca al verdadero
//compass routing
vector<int> wirelessNetwork::compassRouting(int s, int t){
    //Resetiamos el vector de booleanos a falso
    vector<bool> f(G.getNumberVertices(), false);
    visitados = f;

    return compassRouting2(s,t);
}

//Es invocado por una funcion "de ayuda" para la recursion
//Halla el camino mas corto del nodo s a t por medio de
//compass routing; esto es, buscando los nodos n que hagan
//el angulo nst mas corto (donde s es el vertice)
vector<int> wirelessNetwork::compassRouting2(int s, int t){
    //Caso Base: Si el indice s es el mismo indice t, el cual
    //buscamos, entonces creamos un vector vacio y lo enviamos
    if (s == t){
        vector<int> path;
        path.push_back(t);

        return path;
    }
    else{

        //Creamos un vector de enteros para tener acceso a los vecinos
        //del vertice s
        vector<int> sNeigh = G.getNeighbors(s);

        //Si el vertice s no tiene vecinos, devolvemos un
        //vector vacio
        if (sNeigh.size()==0){
            vector<int> rutaNula;

            return rutaNula;
        }
        //Si el unico vertice vecino de s ya fue visitado...
        else if ((sNeigh.size()==1) && visitados[s]){
            vector<int> rutaNula;

            return rutaNula;
        }
        //Si el vertice ya habia sido visitado...
        else if(visitados[s]){
            vector<int> rutaNula;

            return rutaNula;
        }
        //Si no salio de la recusion en los casos anteriores no queda
        //mas que invocar la funcion con el vertice que forme el
        //angulo mas pequeno con con s y t, tomando como vertice del
        //angulo a s. Aqui usamos la ley de coseno para determinar
        //el grado de un triangulo dados los lados.
        else{
            double maxCos = -2; //Coseno da valores entre -1 y 1, por
            //lo tanto desde el primer valor evaluado se obtendra un
            //grado mayor (Coseno grande => grado pequeno)

            //Este sera el proximo vertice al que le realizaremos
            //compassRouting con t
            int next = s;

            //Buscamos quien de los vecinos de s hace el grado menor
            for (int i=0; i<(int)sNeigh.size(); i++){
                int neigh = sNeigh[i];

                if (angle(G.Vertices[s], G.Vertices[t],
                           G.Vertices[neigh]) > maxCos){
                    maxCos = angle(G.Vertices[s], G.Vertices[t],
                                   G.Vertices[neigh]);
                    next = neigh;
                }
            }

            //Marcamos a s como visitado
            visitados[s] = true;
            return merge(compassRouting2(next, t), s);
        }
    }
}
