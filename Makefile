experiments:  mygraph.o wirelessnetwork.o experiments.o
	g++ -o experiments mygraph.o wirelessnetwork.o experiments.o

mygraph.o: mygraph.cpp mygraph.h
	g++ -c mygraph.cpp

wirelessnetwork.o: wirelessnetwork.cpp wirelessnetwork.h
	g++ -c wirelessnetwork.cpp

experiments.o: experiments.cpp
	g++ -c experiments.cpp

clean:
	rm *.o experiments antes.dot despues.dot
