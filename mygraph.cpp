/******MYGRAPH.CPP*******************************************************
 *      Esta es la implementacion de la clase myGraph.                  *
 *                                                                      *
 ************************************************************************
 ************************************************************************
 * est. Ramon L Collazo     801-12-1480     rlcmartis@gmail.com         *
 * Prof. Arce Nazario       Estructura de Datos       CCOM-3034         *
 ************************************************************************/

#include "wirelessnetwork.h"
#include "mygraph.h"
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <stdlib.h>
#include <ctime>
using namespace std;

//Devuelve el indice del -primer- nodo llamado st
//Si no existe devuelve -1
int myGraph::findNodeIndex(string st){
    for (int i = 0; i<(int)Vertices.size(); i++){
        if (Vertices[i].data == st)
            return i;
    }
    return -1;//De no encontrarse vertice con data st
    //devolvemos -1 para mostrar que no se hallo ya
    //que no existen indices negativos
}

//Anade vertice de nombre "" al grafo en (a,b)
void myGraph::addVertex(double a, double b){
    Vertex A(a, b);
    Vertices.push_back(A);
}

//Anade vertice de nombre st al grafo en (a,b)
void myGraph::addVertex(string st, double a, double b){
    Vertex A(st, a, b);
    Vertices.push_back(A);
}

//Anade arista de st1 a st2 con su respectivo peso
//Recordar: se invoca a findNodeIndex (ver descripcion)
void myGraph::addEdge(string st1, string st2, double w){
    int a = findNodeIndex(st1);
    int b = findNodeIndex(st2);
    if (a==-1 || b==-1){
        cout << "Error, no hay ningun nodo de nombre ";
        if (a==-1) cout << st1 << endl;
        else cout << st2 << endl;
        exit(1); //Si no existe el vertice podemos llegar
        //a un grave error, por lo que salimos abruptamente
    }
    else addEdge(a, b, w);
}

//Anade arista desde el indice 'from' hasta el 'to'
//con su respectivo peso
void myGraph::addEdge(int from, int to, double w){
    //Dado a que es un grafo sin direccion y estamos usando
    //el formato de grafos direccionados debemos crear una
    //arista de from a to y de to a from
    AdjacencyAndWeight A(from, w);
    Vertices[to].AWList.push_back(A);

    AdjacencyAndWeight B(to, w);
    Vertices[from].AWList.push_back(B);
}

//Elimina el vertice por su nombre
//Recordar: se invoca a findNodeIndex (ver descripcion)
void myGraph::deleteVertex(string st){
    int a = findNodeIndex(st);
    if (a==-1){
        cout << "Error, no hay ningun nodo de nombre " << st;
        exit(1);//Si no existe el vertice podemos llegar
        //a un grave error, por lo que salimos abruptamente
    }
    else deleteVertex(a);
}

//Elimina vertice segun su indice
void myGraph::deleteVertex(int index){
    //Debemos recordar borrar las aristas del vertice
    for (int i = 0; i < (int)Vertices[index].AWList.size(); i++)
        deleteEdge(index, Vertices[index].AWList[i].adjacent);

    Vertices.erase(Vertices.begin() + index);
}

//Elimina aristas segun su nombre
//Recordar: se invoca a findNodeIndex (ver descripcion)
void myGraph::deleteEdge(string st1, string st2){
    int a = findNodeIndex(st1);
    int b = findNodeIndex(st2);

    if (a==-1 || b==-1){
        cout << "Error, no hay ningun nodo de nombre ";
        if (a==-1) cout << st1 << endl;
        else cout << st2 << endl;
        exit(1);//Si no existe el vertice podemos llegar
        //a un grave error, por lo que salimos abruptamente
    }
    else deleteEdge(a, b);
}

//Elimina aristas segun sus indices
void myGraph::deleteEdge(int from, int to){
    int f = -1, t = -1;
    for(int i = 0; i < (int)Vertices[from].AWList.size(); i++)
      if(Vertices[from].AWList[i].adjacent == to)
        f = i;
    for(int i = 0; i < (int)Vertices[to].AWList.size(); i++)
      if(Vertices[to].AWList[i].adjacent == from)
        t = i;
    if(f > -1 && t > -1){
        Vertices[from].AWList.erase(Vertices[from].AWList.begin() + f);
        Vertices[to].AWList.erase(Vertices[to].AWList.begin() + t);
    }

//    //Borramos adyacencia de from hacia to
//    for (int i=0; i<(int)Vertices[from].AWList.size(); i++)
//        if (Vertices[from].AWList[i].adjacent == to){
//            Vertices[from].AWList.erase(
//                        Vertices[from].AWList.begin() + i);
//            break;
//        }

//    //Borramos adyacencia de to hacia from
//    for (int i=0; i<(int)Vertices[to].AWList.size(); i++)
//        if (Vertices[to].AWList[i].adjacent == from){
//            Vertices[to].AWList.erase(
//                        Vertices[to].AWList.begin() + i);
//            break;
//        }

}

//Devuelve el nombre del vertice index
string myGraph::getData(int index){
    return Vertices[index].data;
}

//Devuelve la adyacencia de indice a, del vertice con indice v
int myGraph::getAdjacent(int v, int a){
    return Vertices[v].AWList[a].adjacent;
}

//Despliega el grafo en pantalla en formato propio del autor
void myGraph::display(){
    for (int i=0; i<(int)Vertices.size(); i++){
        cout << i << " " << Vertices[i].data
             << " (" << Vertices[i].x << ","
                << Vertices[i].y << "): ";
        for (int j=0; j<(int)Vertices[i].AWList.size(); j++){
            cout << "[" << Vertices[i].AWList[j].adjacent
                 << ": " << Vertices[i].AWList[j].weight << "] ";
        }
        cout << endl;
    }
}

//Devuelve un vector de indices enteros, estos indices
//son los vecinos del vertice de indice i
vector<int> myGraph::getNeighbors(int i){
    vector<int> V;
    for (int j=0; j<(int)Vertices[i].AWList.size();j++)
        V.push_back(Vertices[i].AWList[j].adjacent);
    return V;
}

//Devuelve el numero de vertices que contiene el grafo
int myGraph::getNumberVertices(){
    return (int)Vertices.size();
}

//Devuelve el numero de aristas que contiene el grafo
int myGraph::getNumberEdges(){
    int total = 0;
    for (int i=0; i<(int)Vertices.size(); i++){
        total += (int)Vertices[i].AWList.size();
    }
    return total/2;
}

//Devuelve el numero de aristas del vertice index
int myGraph::getNumberEdges(int index){
    return (int)Vertices[index].AWList.size();
}
