/******MYGRAPH.H*********************************************************
 *      Este es el "header" de la clase myGraph. Esta es un grafo no    *
 * dirigido, lo que implica que posee miembros vertices con aristas     *
 * y demas funciones relacinadas a esta.                                *
 *                                                                      *
 ************************************************************************
 ************************************************************************
 * est. Ramon L Collazo     801-12-1480     rlcmartis@gmail.com         *
 * Prof. Arce Nazario       Estructura de Datos       CCOM-3034         *
 ************************************************************************/

#ifndef MYGRAPH_H
#define MYGRAPH_H

#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <ctime>
#include <string>
using namespace std;

class AdjacencyAndWeight{
    public:
        int adjacent; //Vertice vecino (unido con arista)
        double weight; //Peso de arista del vertice original
        //al adjacent (peso equivale a distancia en este proyecto)

        //AdjacencyAndWeight(); Este constructor lo queremos evitar
        //Utilizamos el inferior porque requeriremos tal informacion
        AdjacencyAndWeight(int a, double b)
            {adjacent = a; weight = b;};
};

class Vertex{
    public:
        string data; //Nombre del vertice
        double x; //Coordenada x en el grafo
        double y; //Coordenada y en el grafo
        vector<AdjacencyAndWeight> AWList; //Lista de adyacencias del
        //vertice con peso incluido (que seria la distancia)

        //Queremos, a lo minimo, las coordenadas del vertice por lo que
        //Vertex(); es un constructor que queremos evitar
        Vertex(double a, double b) {data=""; x=a; y=b;};
        Vertex(string st, double a, double b) {data=st; x=a; y=b;};
};

class myGraph{
    protected:
        //Un grafo es el conjunto de vertices y aristas
        //Por lo tanto, tenemos la lista de vertices y
        //en cada vertice las aristas que le inciden
        vector<Vertex> Vertices;

    public:
        //Constructor de la clase (no hace nada especifico)
        myGraph(){};

        //Devuelve el indice del -primer- nodo llamado st
        //Si no existe devuelve -1
        int findNodeIndex(string st);

        //Anade vertice de nombre "" al grafo en (a,b)
        void addVertex(double a, double b);

        //Anade vertice de nombre st al grafo en (a,b)
        void addVertex(string st, double a, double b);

        //Anade arista de st1 a st2 con su respectivo peso
        //Recordar: se invoca a findNodeIndex (ver descripcion)
        void addEdge(string st1, string st2, double w);

        //Anade arista desde el indice 'from' hasta el 'to'
        //con su respectivo peso
        void addEdge(int from, int to, double w);

        //Elimina el vertice por su nombre
        //Recordar: se invoca a findNodeIndex (ver descripcion)
        void deleteVertex(string st);

        //Elimina vertice segun su indice
        void deleteVertex(int index);

        //Elimina aristas segun su nombre
        //Recordar: se invoca a findNodeIndex (ver descripcion)
        void deleteEdge(string st1, string st2);

        //Elimina aristas segun su indice
        void deleteEdge(int from, int to);

        //Devuelve el nombre del vertice index
        string getData(int index);

        //Devuelve la adyacencia de indice a, del vertice con indice v
        int getAdjacent(int v, int a);

        //Despliega el grafo en pantalla en formato propio del autor
        void display();

        //Devuelve un vector de enteros con los indices de los vertices
        //adyacentes al vertice i
        vector<int> getNeighbors(int i);

        //Devuelve el numero de vertices que contiene el grafo
        int getNumberVertices();

        //Devuelve el numero de aristas que contiene el grafo
        int getNumberEdges();

        //Devuelve el numero de aristas del vertice index
        int getNumberEdges(int index);

        //Dado a que wirelessNetwork estará utilizando constantemente
        //esta clase, lo declaramos amigo para que acceda sin problemas
        //las partes protegidas
        friend class wirelessNetwork;
};

#endif // MYGRAPH_H
